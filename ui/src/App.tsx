import React, {FunctionComponent, useEffect, useState} from 'react';
import useDebounce from './useDebounce';
import StockChart from './StockChart';

const App:FunctionComponent<{ initial?: string }> = ({ initial = '' }) => {
  const [symbol, setSymbol] = useState(initial);
  const [results, setResults] = useState({
      data: {},
      metaData: {},
  });
  const [isSearching, setIsSearching] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const debouncedSearchTerm = useDebounce(symbol, 500);

    useEffect(
        // @ts-ignore
        () => {
          const getStocks = async () => {
              try {
                  const result: any = await fetch(`http://localhost:3000/api/v1/stocks/${debouncedSearchTerm}`);
                  const data = await result.json();
                  setIsSearching(false);
                  if(!data || !data.error) {
                      setResults(data);
                  } else {
                      setErrorMessage('Symbol not found');
                  }
              } catch(e) {
                  setResults({ data: {}, metaData: {}});
                  setIsSearching(false);
              }
          }
        if (debouncedSearchTerm) {
          setIsSearching(true);
          getStocks();
        } else {
            setResults({ data: {}, metaData: {}});
        }
      },

      [debouncedSearchTerm]
  );

  return <div>
      <div style={{ margin: "auto", width: "20%", padding: "10px"}}>
          Symbol
          <input
              type="text"
              value={symbol}
              onChange={e => setSymbol(e.target.value)}
              style={{margin: "5px"}}
          />
          {isSearching && <div>Searching ...</div>}
          {errorMessage && <div> { errorMessage } </div>}
      </div>
    {Object.keys(results.data).length > 0 &&
        <StockChart
            data={results.data}
            metaData={results.metaData}
        />
    }
  </div>
}

export default App;