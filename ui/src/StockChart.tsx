import React, {FunctionComponent} from 'react'
import Highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official'

// @ts-ignore


const StockChart:FunctionComponent<{ data: object, metaData: any }> = ({ data, metaData }) => {
    const stockName = `${metaData["2. Symbol"]} Stock Price`
    const options = {
        title: {
            text: stockName
        },
        series: [{
            name: metaData["2. Symbol"],
            data: data,
            tooltip: {
                valueDecimals: 2
            }
        }]
    }
    return <div>
        <HighchartsReact
            highcharts={Highcharts}
            options={options}
            constructorType={'stockChart'}
        />
    </div>
}

export default StockChart;
