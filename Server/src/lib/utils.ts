const transform = (data: object): object => {
    const array = [];
    for (let prop in data ) {
        if (prop.includes('Time Series')) {
            for ( let p in data[prop] ) {
                for(let key in data[prop][p]) {
                    if(key.includes('close')) {
                        array.push([new Date(p).getTime(), Number(data[prop][p][key])]);
                    }
                }
            }
        }
    }
    return { metaData: data['Meta Data'], data: array };
};

module.exports = {
    transform: transform
};