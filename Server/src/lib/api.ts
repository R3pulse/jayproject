const utils = require('./utils');
class Api {
    private alpha: any;
    private data: object;

    constructor(apiKey: string) {
        this.alpha = require('alphavantage')({ key:  apiKey });
    };

    async getStocks(symbol: string) {
        try {
            return utils.transform(await this.alpha.data.daily(symbol));
        } catch(error) {
            return { error: 'Symbol not found'};
        }
    }
}

module.exports = Api