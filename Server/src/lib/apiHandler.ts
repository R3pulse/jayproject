let alphaApi = require('./api');
const api = new alphaApi(process.env.ALPHA_KEY || 'E2PER3ORL947KPCA');


exports.getStocks = async (req, res, next): Promise<any> => {
        const stocks = await api.getStocks(req.params.symbol);
        res.json(stocks);
}
