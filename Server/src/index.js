// import express, { Application, Request, Response, NextFunction } from 'express';
// import { writeFileSync } from "fs"
const express = require('express');
const api = require('./lib/apiHandler');
const app = express();
const port = 4000;

app.get('/', (req, res) => {
    res.json({ 200: "ok" });
});
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
