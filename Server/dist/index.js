const express = require('express');
const cors = require('cors');
const apiHandler = require('./lib/apiHandler');
const app = express();
const port = process.env.PORT || 4000;
app.use(cors());
app.get('/', (req, res) => {
    res.json({ 200: "ok" });
});
app.get('/api/v1/stocks/:symbol', cors(), apiHandler.getStocks);
app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});
