"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const app = express_1.default();
const port = 3000;
const api = require('./lib/apiHandler');
app.get('/', (req, res) => {
    res.json({ 200: "ok" });
});
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
