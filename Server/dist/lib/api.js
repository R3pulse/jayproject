const utils = require('./utils');
class Api {
    constructor(apiKey) {
        this.alpha = require('alphavantage')({ key: apiKey });
    }
    ;
    async getStocks(symbol) {
        try {
            return utils.transform(await this.alpha.data.daily(symbol));
        }
        catch (error) {
            return { error: 'Symbol not found' };
        }
    }
}
module.exports = Api;
