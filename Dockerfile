FROM node:14 AS ui-build
WORKDIR /usr/src/app
COPY ui/ ./ui/
RUN cd ui && npm install && npm run build

FROM node:14 AS server-build
WORKDIR /root/
COPY --from=ui-build /usr/src/app/ui/build ./ui/build
COPY Server/package*.json ./
COPY Server/tsconfig*.json ./
COPY Server/src ./src
RUN npm ci --quiet && npm run build

EXPOSE 4000

CMD ["node", "./dist/index.js"]
